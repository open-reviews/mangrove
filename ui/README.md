# Mangrove Demo UI

UI used to demonstrate the Mangrove Open Reviews capabilities, see other integrations [here](https://open-reviews.net/about/ecosystem/).

Both servers need to be up for the UI to work.

## Build Setup

``` bash

# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate

# run frontend tests
$ yarn run test
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
