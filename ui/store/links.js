export const LINKS = {
  Mastodon: {
    icon: 'mdi-mastodon',
    link: 'https://mas.to/@mangroveReviews'
  },
  Element: {
    icon: 'mdi-chat',
    link: 'https://matrix.to/#/#openreviews:matrix.org'
  },
  Gitlab: {
    icon: 'mdi-gitlab',
    link: 'https://gitlab.com/open-reviews/mangrove'
  },
  OpenCollective: {
    link: 'https://opencollective.com/mangrove'
  },
  Email: {
    icon: 'mdi-email',
    link: 'mailto:hello@open-reviews.net'
  }
}
