# Mangrove Open Reviews

[See the Demo UI deployment](https://mangrove.reviews) or [learn more about the project](https://open-reviews.net). There are also [other projects](https://open-reviews.net/about/ecosystem/) which build upon the servers deployed here.

This repository contains:

- [Mangrove Review Standard](ui/content/Mangrove_Review_Standard.md)
- [Mangrove Original Services Terms of Use](ui/content/Mangrove_Original_Services_ToS.md)
- Mangrove Original Server implementation as [Lambda](https://aws.amazon.com/lambda/)
  using [Axum](https://github.com/tokio-rs/axum)
    - [Reviewer Lambda](servers/reviewer) accessing PostgreSQL database
    - [File Hoster Lambda](servers/file_hoster) accessing an S3 bucket
- [Mangrove Demo UI](ui) implementation
    - Client using Vue.js
- [Mangrove Client JS Library](libraries/mangrove-reviews-js) for interacting with Mangrove Servers
- [Mangrove Aggregator](aggregator) a probabilistic model to help with making sense of reviews

Individual READMEs contain more information.

## Architecture

![infrastructure](img/infrastructure.svg)