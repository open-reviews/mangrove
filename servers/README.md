# Mangrove Servers

[Rust compiler](https://rustup.rs/) is needed to develop these.

## Deploying to AWS Lambda

Deployment can be done using AWS Serverless Application
Model ([AWS SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html)). The
required CloudFormation template is already set up in [aws-template.yaml](aws-template.yaml).

Requirements:

- [cargo-lambda tool](https://www.cargo-lambda.info/)
- [AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/install-sam-cli.html)
- An existing S3 bucket
- Predefined aws credentials and default region

Adjust the name of S3 buckets in the configuration files:

```
./servers/file_hoster/samconfig.toml
./servers/reviewer/samconfig.toml
```

Run command to build lambda:

```
cargo lambda build --bin reviewer --release --x86-64 -o Zip
```

Run command to deploy server on aws:

```
sam deploy --force-upload
```

Add `--guided` parameter to `sam deploy` to follow the questionnaire of parameters instead of using file settings.

### Update lambda function code

Requirements:

- [AWS CLI](https://aws.amazon.com/cli/)

Use the following command for updating lambda function code:

```
aws lambda update-function-code --function-name <function_name> --zip-file fileb://./target/lambda/reviewer/bootstrap.zip --no-publish --no-cli-pager
```