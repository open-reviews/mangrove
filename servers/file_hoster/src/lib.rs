mod errors;

use aws_config::BehaviorVersion;
use aws_sdk_s3::Client;
use axum::extract::{DefaultBodyLimit, Multipart, State};
use axum::http::Method;
use axum::routing::get;
use axum::Json;
use axum::Router;
use errors::Error;

use axum::body::Bytes;
use sha2::{Digest, Sha256};
use std::io::{self};

use tower_http::cors::{Any, CorsLayer};

const SERVER_NAME: &str = "Mangrove";

const BUCKET: &str = "files.mangrove.reviews";

const MULTIPART_FORM_DATA_MAX_SIZE: usize = 25 * 1024 * 1024;
const MAX_FILES_QUANTITY: usize = 5;

/// Compute base64url encoded SHA256 of the file.
fn hash(data: &File) -> Result<String, io::Error> {
    let mut hasher = Sha256::new();
    let mut file = data.bytes.as_ref();
    io::copy(&mut file, &mut hasher)?;
    Ok(base64_url::encode(&hasher.finalize()[..]))
}

// trait HashingStore {
//     // Put file in temporary storage under its hash and return hash.
//     fn save(&self, data: &File) -> Result<String, String>;
// }

// TODO: used for local testing purposes
/// Works by using the root Path for temporary files.
// impl HashingStore for &Path {
//     fn save(&self, data: &SingleFileField) -> Result<String, String> {
//         let hash = hash(data).map_err(|e| e.to_string())?;
//         if !self.join(FILES).join(&hash).exists() {
//             let temp_path = self.join(&hash);
//             if !temp_path.exists() {
//                 info!("Saving file: {}", hash);
//                 fs::rename(&data.path, temp_path).map_err(|e| e.to_string())?;
//             }
//         }
//         Ok(hash)
//     }
// }
#[allow(dead_code)]
#[derive(Debug)]
struct File {
    pub filename: Option<String>,
    pub content_type: Option<String>,
    pub bytes: Bytes,
}

async fn upload(
    State(s3_client): State<Client>,
    mut multipart: Multipart,
) -> Result<Json<Vec<String>>, Error> {
    let mut files: Vec<File> = vec![];
    while let Some(field) = multipart.next_field().await? {
        let content_type = field.content_type().map(|val| val.to_string());

        // Validate file to be image file
        if !content_type
            .clone()
            .unwrap_or_default()
            .starts_with("image/")
        {
            continue;
        }

        let filename = match field.file_name() {
            Some(val) => Some(val.to_string()),
            None => field.name().map(|val| val.to_string()),
        };

        let bytes = field.bytes().await?;

        files.push(File {
            filename,
            content_type,
            bytes,
        });
    }

    // Verification quantity of files
    match files.len() {
        0 => {
            return Err(Error::NoPictures(
                "You should upload at least 1 picture".into(),
            ))
        },
        val if val > MAX_FILES_QUANTITY => {
            return Err(Error::MaxFileQuantity(format!(
                "Allow for up to {} pictures to be uploaded",
                MAX_FILES_QUANTITY
            )))
        },
        _ => {},
    }

    let mut vec_result = vec![];

    for file in files.iter() {
        let hash = hash(file)?;

        match s3_client
            .put_object()
            .bucket(BUCKET)
            .key(hash.clone())
            .body(file.bytes.clone().into())
            .content_type(file.content_type.clone().unwrap_or_default())
            .set_acl(Some("public-read".into()))
            .set_tagging(Some("tmp=true".into()))
            .send()
            .await
        {
            Ok(_) => vec_result.push(hash),
            Err(error) => {
                tracing::error!("Error uploading file with hash: {}, Error: {}", hash, error);
                return Err(error.into());
            },
        }
    }

    Ok(Json(vec_result))
}

async fn index() -> &'static str {
    "Check out for project information: https://mangrove.reviews"
}

async fn server_header_middleware(
    req: axum::extract::Request,
    next: axum::middleware::Next,
) -> axum::response::Response {
    let mut res = next.run(req).await;
    res.headers_mut().insert(
        axum::http::header::SERVER,
        axum::http::header::HeaderValue::from_static(SERVER_NAME),
    );
    res
}

async fn app() -> Result<Router, Box<dyn std::error::Error + Send + Sync>> {
    let config = aws_config::load_defaults(BehaviorVersion::latest()).await;
    let client = Client::new(&config);

    let cors = CorsLayer::new()
        .allow_origin(Any)
        .allow_methods([Method::GET, Method::PUT]);

    let app = Router::new()
        .route("/", get(index).put(upload))
        .layer(cors)
        .layer(DefaultBodyLimit::max(MULTIPART_FORM_DATA_MAX_SIZE))
        .layer(axum::middleware::from_fn(server_header_middleware))
        .with_state(client);

    Ok(app)
}

#[cfg(not(debug_assertions))]
pub async fn lambda() -> Result<(), lambda_http::Error> {
    // AWS Runtime can ignore Stage Name passed from json event
    // Remove if you want the first section of the url to be the stage name of the API Gateway
    // i.e with: `GET /test-stage/todo/id/123` without: `GET /todo/id/123`
    std::env::set_var("AWS_LAMBDA_HTTP_IGNORE_STAGE_IN_PATH", "true");

    // required to enable CloudWatch error logging by the runtime
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    let app = app().await?;

    lambda_http::run(app).await?;

    Ok(())
}

#[cfg(debug_assertions)]
pub async fn run(
    addr: impl Into<std::net::SocketAddr>,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let addr: std::net::SocketAddr = addr.into();

    let app = app().await?;

    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    axum_server::bind(addr)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}
