#[cfg(debug_assertions)]
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Sync + Send>> {
    let port_number = std::env::var("PORT_NUMBER").unwrap_or("8000".to_string());
    file_hoster::run(([0, 0, 0, 0], port_number.parse::<u16>()?)).await?;

    Ok(())
}

#[cfg(not(debug_assertions))]
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Sync + Send>> {
    file_hoster::lambda().await?;

    Ok(())
}
