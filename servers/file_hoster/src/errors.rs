use aws_sdk_s3::error::SdkError;
use aws_sdk_s3::operation::put_object::PutObjectError;
use axum::extract::multipart::MultipartError;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};

#[derive(Debug)]
pub enum Error {
    Multipart(MultipartError),
    MaxFileQuantity(String),
    NoPictures(String),
    S3Bucket(String),
    IO(String),
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        match self {
            Error::Multipart(error) => (StatusCode::BAD_REQUEST, error.to_string()).into_response(),
            Error::MaxFileQuantity(error) => (StatusCode::BAD_REQUEST, error).into_response(),
            Error::S3Bucket(error) => (StatusCode::INTERNAL_SERVER_ERROR, error).into_response(),
            Error::IO(error) => (StatusCode::INTERNAL_SERVER_ERROR, error).into_response(),
            Error::NoPictures(error) => (StatusCode::BAD_REQUEST, error).into_response(),
        }
    }
}

impl From<MultipartError> for Error {
    fn from(error: MultipartError) -> Self {
        Self::Multipart(error)
    }
}

impl From<SdkError<PutObjectError>> for Error {
    fn from(error: SdkError<PutObjectError>) -> Self {
        Self::S3Bucket(error.to_string())
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Self::IO(error.to_string())
    }
}
