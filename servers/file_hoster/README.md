# Mangrove File Hoster

An extra service required to support file upload via the Mangrove Demo UI. It allows for file upload to S3.

To develop just run:
```
cargo run
```

## Endpoints:

- `/`: `PUT` Upload files to store them on the server, get the list of SHA256 hash of each file if successful. Maximum
  of 5 pics can be sent. Maximum multipart-form data size is 25Mb per request.
- `/`: `GET` returning index page.

