use crate::aggregator::Issuer;
use crate::aggregator::Subject;
use crate::fetch::Reviews;
use crate::review::UncertainPoint;
use crate::review::{Payload, Review};
use crate::server::routes::batch::__path_batch;
use crate::server::routes::batch::{BatchQuery, BatchReturn};
use crate::server::routes::coordinates::__path_get_reviews_by_coordinates;
use crate::server::routes::index::__path_index;
use crate::server::routes::issuer::__path_get_issuer;
use crate::server::routes::review::__path_get_review_json;
use crate::server::routes::reviews::__path_get_reviews_route;
use crate::server::routes::subject::__path_get_subject;
use crate::server::routes::submit::__path_submit_review_jwt;
use utoipa::OpenApi;

#[derive(OpenApi)]
#[openapi(
paths(
index,
submit_review_jwt,
get_reviews_route,
get_review_json,
get_subject,
get_issuer,
batch,
get_reviews_by_coordinates
),
components(schemas(Reviews, Review, Subject, Issuer, BatchQuery, BatchReturn, UncertainPoint, Payload)),
info(
title = "Mangrove Server API",
description = "Submit and retrieve reviews,
    as well as aggregate statistics about reviews.
    More information about the values and types can be found in the
    [Open Reviews Standard](https://mangrove.reviews/standard).
    For more description and example usage see [Mangrove Demo UI](https://mangrove.reviews)
    and [Mangrove Client JS Library](https://js.mangrove.reviews/)
    for a JavaScript wrapper of this API.",
contact(
name = "Open Reviews Association",
email = "hello@open-reviews.net"
)
),
servers((url = "https://api.mangrove.reviews"))
)]
pub struct ApiDoc;

pub fn generate() -> anyhow::Result<String> {
    ApiDoc::openapi().to_yaml().map_err(Into::into)
}
