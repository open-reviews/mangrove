// @generated automatically by Diesel CLI.

diesel::table! {
    use diesel::sql_types::*;
    use postgis_diesel::sql_types::*;

    reviews (signature) {
        signature -> Text,
        jwt -> Text,
        kid -> Text,
        iat -> Int8,
        sub -> Text,
        rating -> Nullable<Int2>,
        opinion -> Nullable<Text>,
        images -> Nullable<Json>,
        metadata -> Nullable<Json>,
        scheme -> Text,
        coordinates -> Nullable<Geography>,
        uncertainty -> Nullable<Int4>,
    }
}
