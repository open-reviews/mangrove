use super::aggregator::{Issuer, Statistic, Subject};
use super::database::{DbConn, Query};
use super::error::Error;
use super::review::Review;
use serde::Serialize;
use std::collections::{BTreeMap, HashSet};

pub type Subjects = BTreeMap<String, Subject>;
pub type Issuers = BTreeMap<String, Issuer>;

/// Return type used to provide `Review`s and any associated data.
#[derive(Debug, Serialize)]
pub struct Reviews {
    /// A list of reviews satisfying the `Query`.
    pub reviews: Vec<Review>,
    // Adding pub to issuers/maresi_subjects to be able to configure them from other modules;
    /// A map from public keys to information about issuers.
    pub issuers: Option<Issuers>,
    /// A map from Review identifiers (`urn:maresi:<signature>`)
    /// to information about the reviews of that review.
    pub maresi_subjects: Option<Subjects>,
}

impl<'__s> utoipa::ToSchema<'__s> for Reviews {
    fn schema() -> (
        &'__s str,
        utoipa::openapi::RefOr<utoipa::openapi::schema::Schema>,
    ) {
        (
            "Reviews",
            utoipa::openapi::ObjectBuilder::new()
                .description(Some("Return type used to provide `Review`s and any associated data."))
                .property(
                    "issuers",
                    utoipa::openapi::ObjectBuilder::new()
                        .description(Some("A map from public keys to information about issuers."))
                        .additional_properties(Some(utoipa::openapi::RefOr::Ref(utoipa::openapi::Ref::new("#/components/schemas/Issuer"))))
                        .nullable(true),
                )
                .required("issuers")
                .property(
                    "maresi_subjects",
                    utoipa::openapi::ObjectBuilder::new()
                        .description(Some("A map from Review identifiers (`urn:maresi:<signature>`) to information about the reviews of that review."))
                        .additional_properties(Some(utoipa::openapi::RefOr::Ref(utoipa::openapi::Ref::new("#/components/schemas/Subject"))))
                        .nullable(true),
                )
                .required("maresi_subjects")
                .property(
                    "reviews",
                    utoipa::openapi::ArrayBuilder::new()
                        .description(Some("A list of reviews satisfying the query."))
                        .items(
                            utoipa::openapi::RefOr::Ref(utoipa::openapi::Ref::new("#/components/schemas/Review"))
                        ),
                )
                .required("reviews")
                .into(),
        )
    }
}

pub async fn get_reviews(conn: DbConn, query: Query) -> Result<Reviews, Error> {
    println!("Reviews requested for query {:?}", query);
    let add_issuers = query.issuers.unwrap_or(false);
    let add_subjects = query.maresi_subjects.unwrap_or(false);
    let reviews = conn.filter(query).await?;
    let out = Reviews {
        issuers: if add_issuers {
            Some(
                Issuer::compute_bulk(
                    &conn,
                    reviews
                        .iter()
                        .map(|review| review.kid.clone())
                        // Deduplicate before computing Issuers.
                        .collect::<HashSet<_>>()
                        .into_iter(),
                )
                .await?,
            )
        } else {
            None
        },
        maresi_subjects: if add_subjects {
            Some(
                Subject::compute_bulk(
                    &conn,
                    reviews
                        .iter()
                        .map(|review| format!("urn:maresi:{}", review.signature.clone()))
                        // Deduplicate before computing Subjects.
                        .collect::<HashSet<_>>()
                        .into_iter(),
                )
                .await?,
            )
        } else {
            None
        },
        reviews,
    };
    println!("Returning {:?}", out);
    Ok(out)
}
