use super::Connection;

lazy_static::lazy_static! {
    pub static ref MIGRATIONS: diesel_async_migrations::EmbeddedMigrations =
    diesel_async_migrations::embed_migrations!();
}

pub async fn run_pending_migrations(conn: &mut Connection) -> anyhow::Result<()> {
    MIGRATIONS.run_pending_migrations(conn).await?;

    Ok(())
}
