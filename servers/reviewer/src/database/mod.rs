#![allow(unused_imports)]

mod connections_pool;
#[cfg(debug_assertions)]
mod migrations;

use super::error::Error;
use super::review::{validate_sub, Review};
use super::schema;
use anyhow::anyhow;
use async_once::AsyncOnce;
use aws_config::BehaviorVersion;
use diesel::prelude::*;
use diesel::sql_types::{Bool, Float, Integer, Nullable};
use diesel_async::RunQueryDsl;
use lazy_static::lazy_static;
#[cfg(debug_assertions)]
use migrations::run_pending_migrations;

use postgis_diesel::sql_types::{Geography, Geometry};
use postgis_diesel::types::Point;
use serde::{de, Deserialize, Deserializer};
use url::Url;

use crate::fetch::Reviews;
pub use connections_pool::{create_connections_pool, Connection, ConnectionsPool};

#[cfg(debug_assertions)]
pub const DATABASE_URL_ENV_VARIABLE: &str = "DATABASE_URL";
#[cfg(not(debug_assertions))]
pub const DATABASE_URL_SECRET_NAME: &str = "DATABASE_URL";

#[cfg(not(debug_assertions))]
lazy_static! {
    static ref SECRET_MANAGER: AsyncOnce<aws_sdk_secretsmanager::Client> = AsyncOnce::new(async {
        let config = aws_config::load_defaults(BehaviorVersion::latest()).await;
        aws_sdk_secretsmanager::Client::new(&config)
    });
}

#[derive(Clone)]
pub struct DbConn {
    pool: ConnectionsPool,
}

impl DbConn {
    #[cfg(debug_assertions)]
    /// Initialize struct
    pub async fn try_new() -> anyhow::Result<Self> {
        let database_url = std::env::var(DATABASE_URL_ENV_VARIABLE)?;
        let pool = create_connections_pool(database_url.as_str()).await?;
        let mut conn = pool.get().await?;
        run_pending_migrations(&mut conn).await?;
        Ok(Self { pool })
    }

    #[cfg(not(debug_assertions))]
    /// Initialize struct
    pub async fn try_new() -> anyhow::Result<Self> {
        let secret_value = SECRET_MANAGER
            .get()
            .await
            .get_secret_value()
            .secret_id(DATABASE_URL_SECRET_NAME)
            .send()
            .await?;

        if let Some(secret) = secret_value.secret_string {
            let pool = create_connections_pool(secret.as_str()).await?;
            Ok(Self { pool })
        } else {
            Err(anyhow!("{} not found.", DATABASE_URL_SECRET_NAME))
        }
    }

    /// Get the database connection from the pool
    pub async fn get_connection(&self) -> anyhow::Result<Connection> {
        Ok(self.pool.get().await?)
    }
}

/// Query parameters for requesting Reviews
#[derive(Default, Debug, Deserialize, utoipa::IntoParams)]
#[into_params(parameter_in=Query)]
pub struct Coordinates {
    /// Minimum longitude
    pub xmin: f32,
    /// Minimum latitude
    pub ymin: f32,
    /// Maximum longitude
    pub xmax: f32,
    /// Maximum latitude
    pub ymax: f32,
}

/// Query data used to specify which reviews should be returned.
/// Review fulfills the query if all conditions are satisfied (intersection).
#[derive(Default, Debug, Deserialize)]
pub struct Query {
    /// Search for reviews that have this string in `sub` or `opinion` field.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub q: Option<String>,
    /// Review with this `signature` value.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub signature: Option<String>,
    /// Reviews by issuer with the following PEM public key.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub kid: Option<String>,
    /// Reviews of subjects with the following scheme.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub scheme: Option<String>,
    /// Reviews issued at this UNIX time.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub iat: Option<i64>,
    /// Reviews with UNIX timestamp greater than this.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub gt_iat: Option<i64>,
    /// Reviews of the given subject URI.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub sub: Option<String>,
    /// Reviews with the given rating.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub rating: Option<i16>,
    /// Reviews with the given opinion.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub opinion: Option<String>,
    /// Limit the number of returned results.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub limit: Option<i64>,
    /// Get only reviews with opinion present or not present.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub opinionated: Option<bool>,
    /// Include reviews left for example subjects: 'https://example.com' and 'geo:0,0?q=*u=*'
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub examples: Option<bool>,
    /// Include aggregate information about review issuers.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub issuers: Option<bool>,
    /// Include aggregate information about reviews of returned reviews.
    #[serde(default, deserialize_with = "empty_string_as_none")]
    pub maresi_subjects: Option<bool>,
}

/// Serde deserialization decorator to map empty Strings to None,
fn empty_string_as_none<'de, D, T>(de: D) -> Result<Option<T>, D::Error>
where
    D: Deserializer<'de>,
    T: std::str::FromStr,
    T::Err: std::fmt::Display,
{
    let opt = Option::<String>::deserialize(de)?;
    match opt.as_deref() {
        None | Some("") => Ok(None),
        Some(s) => std::str::FromStr::from_str(s)
            .map_err(de::Error::custom)
            .map(Some),
    }
}

define_sql_function! {
    #[sql_name = "ST_DWithin"]
    fn within(c1: Nullable<Geography>, c2: Nullable<Geography>, u: Nullable<Integer>) -> Bool;
}

define_sql_function! {
    #[sql_name = "ST_MakeEnvelope"]
    fn st_make_envelope(xmin: Float, ymin: Float, xmax: Float, ymax: Float, srid: Integer) -> Nullable<Geography>;
}

const EXAMPLE_HTTPS: &str = "https://example.com";
const EXAMPLE_GEO: Point = Point {
    x: 0.,
    y: 0.,
    srid: Some(4326),
};

impl DbConn {
    const MIN_SYMBOLS_SEARCH: usize = 3;

    pub async fn insert(&self, review: Review) -> Result<(), Error> {
        diesel::insert_into(schema::reviews::table)
            .values(review)
            .execute(&mut self.get_connection().await?)
            .await?;
        Ok(())
    }

    pub async fn filter(&self, query: Query) -> Result<Vec<Review>, Error> {
        use schema::reviews::dsl::*;

        let always_true = Box::new(signature.eq(signature));
        let mut f: Box<dyn BoxableExpression<schema::reviews::table, _, SqlType = Bool>> =
            always_true;
        if let Some(s) = &query.signature {
            f = Box::new(f.and(signature.eq(s)))
        }
        if let Some(s) = &query.scheme {
            f = Box::new(f.and(scheme.eq(s)))
        }
        if let Some(s) = &query.opinionated {
            f = Box::new(f.and(opinion.is_null().ne(s)))
        }
        if let Some(s) = &query.kid {
            f = Box::new(f.and(kid.eq(s)))
        }
        if let Some(s) = &query.iat {
            f = Box::new(f.and(iat.eq(s)))
        }
        if let Some(s) = &query.gt_iat {
            f = Box::new(f.and(iat.gt(s)))
        }
        // Match also in vicinity.
        if let Some(s) = &query.sub {
            let (query_scheme, geo, q) = validate_sub(s).await?;
            if query_scheme == "geo" {
                f = Box::new(f.and(scheme.assume_not_null().eq(query_scheme)));
                let is_close = within(coordinates, geo.coordinates, uncertainty + geo.uncertainty);

                match q {
                    Some(q) if q.len() >= Self::MIN_SYMBOLS_SEARCH => {
                        f = Box::new(f.and(is_close.or(sub.ilike(format!("%q={}%", q)))))
                    },
                    _ => f = Box::new(f.and(is_close)),
                };
            } else {
                f = Box::new(f.and(sub.eq(s)))
            }
        } else {
            // Exclude examples only if not explicitly requested.
            if !query.examples.unwrap_or(false) {
                f = Box::new(
                    f.and(sub.ne(EXAMPLE_HTTPS)).and(
                        scheme
                            .ne("geo")
                            .or(coordinates.assume_not_null().ne(EXAMPLE_GEO)),
                    ),
                )
            }
        }
        if let Some(s) = &query.rating {
            f = Box::new(f.and(rating.assume_not_null().eq(s)))
        }
        if let Some(s) = &query.opinion {
            f = Box::new(f.and(opinion.assume_not_null().eq(s)))
        }
        if let Some(s) = &query.q {
            if !s.is_empty() {
                let pattern = format!("%{}%", s);
                f = Box::new(
                    f.and(
                        sub.ilike(pattern.clone())
                            .or(opinion.assume_not_null().ilike(pattern.clone())),
                    ),
                )
            }
        }

        Ok(reviews
            .filter(f)
            .order(iat.desc())
            .limit(query.limit.unwrap_or(10_000))
            .select((
                signature,
                jwt,
                kid,
                (iat, sub, rating, opinion, images, metadata),
                scheme,
                (coordinates, uncertainty),
            ))
            .load::<Review>(&mut self.get_connection().await?)
            .await?)
    }

    pub async fn select(&self, sig: &str) -> Result<Review, Error> {
        use schema::reviews::dsl::*;

        let review = schema::reviews::table
            .filter(signature.eq(sig))
            .select((
                signature,
                jwt,
                kid,
                (iat, sub, rating, opinion, images, metadata),
                scheme,
                (coordinates, uncertainty),
            ))
            .first::<Review>(&mut self.get_connection().await?)
            .await?;

        Ok(review)
    }

    pub async fn filter_by_coordinates(&self, input: Coordinates) -> Result<Vec<Review>, Error> {
        use crate::schema::reviews::dsl::*;

        Ok(reviews
            .select((
                signature,
                jwt,
                kid,
                (iat, sub, rating, opinion, images, metadata),
                scheme,
                (coordinates, uncertainty),
            ))
            .filter(within(
                coordinates,
                st_make_envelope(input.xmin, input.ymin, input.xmax, input.ymax, 4326),
                0,
            ))
            .load::<Review>(&mut self.get_connection().await?)
            .await?)
    }
}
