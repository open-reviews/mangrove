use deadpool::managed::{PoolConfig, Timeouts};
use diesel_async::pooled_connection::deadpool::{Object, Pool};
use diesel_async::pooled_connection::AsyncDieselConnectionManager;
use diesel_async::AsyncPgConnection;

const CONNECTIONS_POOL_TIMEOUT_MILLIS: u64 = 1000;

pub type ConnectionsPool = Pool<AsyncPgConnection>;
pub type Connection = Object<AsyncPgConnection>;

pub async fn create_connections_pool(database_url: &str) -> anyhow::Result<ConnectionsPool> {
    let manager =
        AsyncDieselConnectionManager::<diesel_async::AsyncPgConnection>::new(database_url);

    let pool_config = PoolConfig {
        timeouts: Timeouts::wait_millis(CONNECTIONS_POOL_TIMEOUT_MILLIS),
        ..Default::default()
    };

    Ok(Pool::builder(manager)
        .config(pool_config)
        .runtime(deadpool::Runtime::Tokio1)
        .build()?)
}
