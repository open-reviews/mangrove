use std::env;

const OPENAPI_FILE_PATH_ENV_VARIABLE: &str = "OPENAPI_FILE_PATH";
const DEFAULT_OPENAPI_FILE_PATH: &str = "./docs/mangrove_openapi.yml";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Sync + Send>> {
    let file_path =
        env::var(OPENAPI_FILE_PATH_ENV_VARIABLE).unwrap_or(DEFAULT_OPENAPI_FILE_PATH.to_owned());
    let doc = reviewer::api_doc::generate()?;
    std::fs::write(file_path, doc.as_bytes())?;
    Ok(())
}
