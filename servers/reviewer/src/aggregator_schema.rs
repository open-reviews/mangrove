diesel::table! {
    subjects (sub) {
        sub -> Text,
        quality -> Int2,
    }
}

diesel::table! {
    reviewers (pem) {
        pem -> Text,
        neutrality -> Float,
    }
}
