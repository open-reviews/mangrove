use axum::http::Method;
use axum::routing::{get, post, put};
use axum::Router;
use std::net::SocketAddr;

mod content_types;
mod headers;
pub(crate) mod routes;
mod types;

use crate::database::DbConn;
pub use crate::server::routes::{
    batch, get_issuer, get_review_json, get_reviews_by_coordinates, get_reviews_route, get_subject,
    index, submit_review_jwt,
};
use tower_http::compression::CompressionLayer;
use tower_http::cors::{Any, CorsLayer};

pub use content_types::*;
pub use headers::*;
pub use types::*;

const SERVER_NAME: &str = "Mangrove";

async fn server_header_middleware(
    req: axum::extract::Request,
    next: axum::middleware::Next,
) -> axum::response::Response {
    let mut res = next.run(req).await;
    res.headers_mut().insert(
        axum::http::header::SERVER,
        axum::http::header::HeaderValue::from_static(SERVER_NAME),
    );
    res
}

async fn app() -> anyhow::Result<Router> {
    let db_conn = DbConn::try_new().await?;

    let cors =
        CorsLayer::new()
            .allow_origin(Any)
            .allow_methods([Method::GET, Method::POST, Method::PUT]);

    let app = Router::new()
        .route("/", get(index))
        .route("/submit/:jwt_review", put(submit_review_jwt))
        .route("/reviews", get(get_reviews_route))
        .route("/review/:signature", get(get_review_json))
        .route("/subject/*sub", get(get_subject))
        .route("/issuer/*pem", get(get_issuer))
        .route("/batch", post(batch))
        .route("/geo", get(get_reviews_by_coordinates))
        .with_state(db_conn)
        .layer(axum::middleware::from_fn(server_header_middleware))
        .layer(cors)
        .layer(CompressionLayer::new().gzip(true).deflate(true));

    Ok(app)
}

pub async fn run(addr: impl Into<SocketAddr>) -> anyhow::Result<()> {
    let addr: SocketAddr = addr.into();
    let app = app().await?;

    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    axum_server::bind(addr)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}

pub async fn lambda() -> Result<(), lambda_http::Error> {
    // AWS Runtime can ignore Stage Name passed from json event
    // Remove if you want the first section of the url to be the stage name of the API Gateway
    // i.e with: `GET /test-stage/todo/id/123` without: `GET /todo/id/123`
    std::env::set_var("AWS_LAMBDA_HTTP_IGNORE_STAGE_IN_PATH", "true");

    // required to enable CloudWatch error logging by the runtime
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    let app = app().await?;
    lambda_http::run(app).await?;

    Ok(())
}

// #[cfg(test)]
// mod tests {
//     //use super::*;
//
//     /*
//     #[test]
//     fn add_columns() {
//         use diesel::prelude::*;
//         use schema::reviews::dsl::*;
//         use diesel::{QueryDsl, ExpressionMethods, pg::Pg};
//
//         let conn = PgConnection::establish(&database_url)
//             .expect(&format!("Error connecting to {}", database_url));
//
//         let target = reviews.filter(sub.like("geo:%"));
//         //diesel::update(target).set(coordinates.eq(sub)).execute(&conn).unwrap();
//         //diesel::update(target).set(uncertainty.eq(sub)).execute(&conn).unwrap();
//     }
//     */
// }
