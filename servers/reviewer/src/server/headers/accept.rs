use axum::async_trait;
use axum::http::request::Parts;
use axum::http::StatusCode;
use std::str::FromStr;

#[derive(Debug)]
pub struct Accept(pub Option<Vec<String>>);

#[async_trait]
impl<S> axum::extract::FromRequestParts<S> for Accept
where
    S: Send + Sync,
{
    type Rejection = StatusCode;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let value: Option<Vec<String>> = parts
            .headers
            .get(axum::http::header::ACCEPT)
            .map(|val| {
                Some(
                    val.to_str()
                        .unwrap_or_default()
                        .split(',')
                        .map(|val| String::from_str(val).unwrap_or_default())
                        .collect::<Vec<_>>(),
                )
            })
            .unwrap_or(None);

        Ok(Self(value))
    }
}
