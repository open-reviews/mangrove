use axum::http::header::{CONTENT_DISPOSITION, CONTENT_TYPE};
use axum::response::IntoResponse;
use std::time::SystemTime;

#[derive(Debug)]
pub struct Csv(pub String);

impl IntoResponse for crate::server::Csv {
    fn into_response(self) -> axum::response::Response {
        let unix_time = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .ok()
            .map(|t| t.as_secs())
            .map_or("unknown".into(), |n| n.to_string());

        let filename = format!("mangrove.reviews_{}.csv", unix_time);
        axum::response::Response::builder()
            .header(CONTENT_TYPE, "text/csv")
            .header(
                CONTENT_DISPOSITION,
                format!("attachment; filename={}", &filename),
            )
            .body(self.0.into())
            .unwrap_or_default()
    }
}
