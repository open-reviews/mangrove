use crate::aggregator::{Statistic, Subject};
use crate::database::DbConn;
use crate::error::Error;
use axum::extract::{Path, State};
use axum::response::IntoResponse;
use axum::Json;

/// Request aggregate information about the subject.
#[utoipa::path(
get,
tag = "Review",
path = "/subject/{sub}",
params(
("sub" = String, Path, description = "Request aggregate information about the subject.", example = "https://nytimes.com"),
),
responses(
(status = 200, description = "Success", content_type = "application/json", body = Subject),
(status = 400, description = "Provided request has incorrect format.", body = String),
(status = 500, description = "Server encountered an error when processing the request.", body = String)
)
)]
pub async fn get_subject(
    State(db_conn): State<DbConn>,
    Path(sub): Path<String>,
) -> Result<impl IntoResponse, Error> {
    Ok(Json(Subject::compute(&db_conn, sub).await?).into_response())
}
