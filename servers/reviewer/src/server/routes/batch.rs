use crate::aggregator::{Issuer, Statistic, Subject};
use crate::database::DbConn;
use crate::error::Error;
use crate::fetch::{Issuers, Subjects};
use axum::extract::State;
use axum::response::IntoResponse;
use axum::Json;
use serde::{Deserialize, Serialize};

/// Query allowing for retrieval of information about multiple subjects or issuers.
#[derive(Debug, Deserialize)]
pub struct BatchQuery {
    /// List of subject URIs to get information about.
    subs: Option<Vec<String>>,
    /// List of issuer public keys to get information about.
    pems: Option<Vec<String>>,
}

impl<'__s> utoipa::ToSchema<'__s> for BatchQuery {
    fn schema() -> (
        &'__s str,
        utoipa::openapi::RefOr<utoipa::openapi::schema::Schema>,
    ) {
        (
            "BatchQuery",
            utoipa::openapi::ObjectBuilder::new()
                .description(Some("Query allowing for retrieval of information about multiple subjects or issuers."))
                .property(
                    "pems",
                    utoipa::openapi::ArrayBuilder::new()
                        .description(Some("List of issuer public keys to get information about."))
                        .items(
                            utoipa::openapi::ObjectBuilder::new()
                                .schema_type(utoipa::openapi::SchemaType::String)
                        )
                        .nullable(true),
                )
                .required("pems")
                .property(
                    "subs",
                    utoipa::openapi::ArrayBuilder::new()
                        .description(Some("A map from Review identifiers (`urn:maresi:<signature>`) to information about the reviews of that review."))
                        .items(
                            utoipa::openapi::ObjectBuilder::new()
                                .schema_type(utoipa::openapi::SchemaType::String)
                                .format(Some(utoipa::openapi::SchemaFormat::Custom("uri".to_string())))
                        )
                        .nullable(true),
                )
                .required("subs")
                .into(),
        )
    }
}

#[derive(Debug, Serialize)]
pub(crate) struct BatchReturn {
    #[serde(skip_serializing_if = "Option::is_none")]
    subjects: Option<Subjects>,
    #[serde(skip_serializing_if = "Option::is_none")]
    issuers: Option<Issuers>,
}

impl<'__s> utoipa::ToSchema<'__s> for BatchReturn {
    fn schema() -> (
        &'__s str,
        utoipa::openapi::RefOr<utoipa::openapi::schema::Schema>,
    ) {
        (
            "BatchReturn",
            utoipa::openapi::ObjectBuilder::new()
                .property(
                    "issuers",
                    utoipa::openapi::ObjectBuilder::new()
                        .additional_properties(Some(utoipa::openapi::RefOr::Ref(
                            utoipa::openapi::Ref::new("#/components/schemas/Issuer"),
                        )))
                        .nullable(true),
                )
                .required("issuers")
                .property(
                    "subjects",
                    utoipa::openapi::ObjectBuilder::new()
                        .additional_properties(Some(utoipa::openapi::RefOr::Ref(
                            utoipa::openapi::Ref::new("#/components/schemas/Subject"),
                        )))
                        .nullable(true),
                )
                .required("subjects")
                .into(),
        )
    }
}

/// Retrieve aggregates for multiple subjects or issuers.
#[utoipa::path(
post,
tag = "Review",
path = "/batch",
responses(
(status = 200, description = "Success", content_type = "application/json", body = BatchReturn),
(status = 400, description = "Provided request has incorrect format.", body = String),
(status = 500, description = "Server encountered an error when processing the request.", body = String)
)
)]
pub async fn batch(
    State(db_conn): State<DbConn>,
    Json(json): Json<BatchQuery>,
) -> Result<impl IntoResponse, Error> {
    println!("Batch request made: {:?}", json);

    let subjects = match &json.subs {
        Some(subs) => Some(Subject::compute_bulk(&db_conn, subs.iter().cloned()).await?),
        None => None,
    };
    let issuers = match json.pems {
        Some(subs) => Some(Issuer::compute_bulk(&db_conn, subs.into_iter()).await?),
        None => None,
    };
    println!(
        "Returning batch of subjects and issuers: {:?} {:?}",
        subjects, issuers
    );
    Ok(Json(BatchReturn { subjects, issuers }).into_response())
}
