use crate::database::{Coordinates, DbConn};
use crate::error::Error;
use crate::fetch::Reviews;
use crate::rdf::IntoRDF;
use crate::server::{Accept, APPLICATION_N_TRIPLES_CONTENT_TYPE_STR};
use axum::extract::Query as QueryExtractor;
use axum::extract::State;
use axum::http::header::CONTENT_TYPE;
use axum::response::IntoResponse;
use axum::Json;

/// Request reviews within provided coordinates.
#[utoipa::path(
get,
tag = "Review",
path = "/geo",
params(Coordinates),
responses(
(status = 200, description="Success", content(("application/json" = Reviews), ("application/n-triples" = String))),
(status = 400, description = "Provided request has incorrect format.", body = String),
(status = 500, description = "Server encountered an error when processing the request.", body = String)
)
)]
pub async fn get_reviews_by_coordinates(
    State(db_conn): State<DbConn>,
    accept: Accept,
    QueryExtractor(coordinates): QueryExtractor<Coordinates>,
) -> Result<impl IntoResponse, Error> {
    println!("Reviews requested by coordinates: {:?}", coordinates);
    let accept = accept.0.unwrap_or(vec![mime::APPLICATION_JSON.to_string()]);

    let reviews = Reviews {
        reviews: db_conn.filter_by_coordinates(coordinates).await?,
        issuers: None,
        maresi_subjects: None,
    };

    println!("Returning reviews by coordinates: {:?}", reviews);
    // Request with accept header application/n-triples
    if accept.contains(&APPLICATION_N_TRIPLES_CONTENT_TYPE_STR.to_string()) {
        Ok((
            [(CONTENT_TYPE, APPLICATION_N_TRIPLES_CONTENT_TYPE_STR)],
            reviews.into_ntriples()?,
        )
            .into_response())
    }
    // Otherwise return application/json
    else {
        Ok(Json(reviews).into_response())
    }
}
