use crate::database::DbConn;
use crate::error::Error;
use crate::rdf::IntoRDF;
use crate::server::{Accept, APPLICATION_N_TRIPLES_CONTENT_TYPE_STR};
use axum::extract::{Path, State};
use axum::http::header::CONTENT_TYPE;
use axum::response::IntoResponse;
use axum::Json;

/// Request review with the specified signature.
#[utoipa::path(
get,
tag = "Review",
path = "/review/{signature}",
params(
("signature" = String, Path, description = "Signature of the review being requested.", example = "PDSpnKtHioXykdBCMA15y5cLuYrRbSexscvySt_ryjppDWaW1I1AijjWercZE6K-cbS18bCwSmgIPqRIuL-cow"),
),
responses(
(status = 200, description = "Success", content(("application/json" = Review), ("application/n-triples" = String))),
(status = 400, description = "Provided request has incorrect format.", body = String),
(status = 404, description = "Signature not found", body = String),
(status = 500, description = "Server encountered an error when processing the request.", body = String)
)
)]
pub async fn get_review_json(
    State(db_conn): State<DbConn>,
    accept: Accept,
    Path(signature): Path<String>,
) -> Result<impl IntoResponse, Error> {
    let accept = accept.0.unwrap_or(vec![mime::APPLICATION_JSON.to_string()]);
    if accept.contains(&APPLICATION_N_TRIPLES_CONTENT_TYPE_STR.to_string()) {
        Ok((
            [(CONTENT_TYPE, APPLICATION_N_TRIPLES_CONTENT_TYPE_STR)],
            db_conn.select(&signature).await?.into_ntriples()?,
        )
            .into_response())
    } else {
        Ok(Json(db_conn.select(&signature).await?).into_response())
    }
}
