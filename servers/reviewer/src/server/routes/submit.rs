use crate::database::DbConn;
use crate::error::Error;
use crate::review::Review;
use axum::extract::{Path, State};
use axum::response::IntoResponse;

/// Submit a new review.
#[utoipa::path(
put,
tag = "Review",
path = "/submit/{jwt_review}",
params(
("jwt_review" = String, Path, example = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ii0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFcDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3cy9aYmdleG41SVQ2bi83NGt2YlZ0UGxNc3A5Z2luTysxMVZ4ZUorbVFJQ1pZamc9PS0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLSIsImp3ayI6IntcImNydlwiOlwiUC0yNTZcIixcImV4dFwiOnRydWUsXCJrZXlfb3BzXCI6W1widmVyaWZ5XCJdLFwia3R5XCI6XCJFQ1wiLFwieFwiOlwicDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3c19aWVwiLFwieVwiOlwiNEhzWi1TRS1wXy0tSkwyMWJUNVRMS2ZZSXB6dnRkVmNYaWZwa0NBbVdJNFwifSJ9.eyJpYXQiOjE1ODA5MTAwMjIsInN1YiI6Imh0dHBzOi8vbWFuZ3JvdmUucmV2aWV3cyIsInJhdGluZyI6NzUsIm9waW5pb24iOiJHcmVhdCB3ZWJzaXRlIGZvciByZXZpZXdzLiIsIm1ldGFkYXRhIjp7Im5pY2tuYW1lIjoiam9objEyMyIsImNsaWVudF9pZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMCJ9fQ.7xQtIlHuDdCVioyztj8i3zJ8dk3oCSfKr6VCR5RtBn6sBcqvpfyvs13PlKGJoamKzx8xUgQTQJjRPv5s91-VLQ", description = "Mangrove Review in JSON Web Token format as described in the [Open Reviews Standard](https://mangrove.reviews/standard). Please use `https://example.com` or `geo:0,0?q=*&u=*` in subject `sub` field for test reviews which should be removed later.")
),
responses(
(status = 200, description = "Review is in the database.", body = inline(bool)),
(status = 400, description = "Provided request has incorrect format.", body = String),
(status = 500, description = "Server encountered an error when processing the request.", body = String)
)
)]
pub async fn submit_review_jwt(
    State(db_conn): State<DbConn>,
    Path(jwt_review): Path<String>,
) -> Result<impl IntoResponse, Error> {
    println!("Review received: {:?}", jwt_review);
    let review = Review::parse(&jwt_review).await?;
    review.validate_db(&db_conn).await?;
    println!("Inserting review: {:?}", review);
    db_conn.insert(review).await?;

    Ok("true")
}
