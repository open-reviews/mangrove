#[utoipa::path(
get,
tag = "Common",
path = "/",
responses(
(status = 200, description = "Mangrove api index page", body = String),
(status = 500, description = "Server encountered an error when processing the request.", body = String)
)
)]
pub async fn index() -> &'static str {
    "More project information: https://mangrove.reviews"
}
