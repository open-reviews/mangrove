use aws_sdk_s3::error::SdkError;
use aws_sdk_s3::operation::delete_object_tagging::DeleteObjectTaggingError;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use sophia::inmem::index::TermIndexFullError;
use sophia::iri::InvalidIri;
use sophia_api::term::bnode_id::InvalidBnodeId;
use std::fmt::Debug;

#[derive(Debug)]
pub enum Error {
    // Issue with the submitted review.
    Incorrect(String),
    // Internal issue when checking a review.
    Internal(String),
    // Not found error.
    NotFound(String),
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        match self {
            Error::Incorrect(message) => (StatusCode::BAD_REQUEST, message),
            Error::Internal(message) => (StatusCode::INTERNAL_SERVER_ERROR, message),
            Error::NotFound(message) => (StatusCode::NOT_FOUND, message),
        }
        .into_response()
    }
}

impl From<anyhow::Error> for Error {
    fn from(anyhow_error: anyhow::Error) -> Self {
        Self::Internal(anyhow_error.to_string())
    }
}

impl From<SdkError<DeleteObjectTaggingError>> for Error {
    fn from(error: SdkError<DeleteObjectTaggingError>) -> Self {
        Self::Incorrect(error.to_string())
    }
}

impl From<jsonwebtoken::errors::Error> for Error {
    fn from(error: jsonwebtoken::errors::Error) -> Self {
        Error::Incorrect(format!("Incorrect JWT: {}", error))
    }
}

impl From<csv::Error> for Error {
    fn from(error: csv::Error) -> Self {
        Error::Internal(format!("Could not encode as CSV: {}", error))
    }
}

impl From<csv::IntoInnerError<csv::Writer<Vec<u8>>>> for Error {
    fn from(error: csv::IntoInnerError<csv::Writer<Vec<u8>>>) -> Self {
        Error::Internal(format!("Could not encode as CSV: {}", error))
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(error: std::string::FromUtf8Error) -> Self {
        Error::Internal(format!("Could not encode CSV as Utf8: {}", error))
    }
}

impl From<diesel::result::Error> for Error {
    fn from(error: diesel::result::Error) -> Self {
        match error {
            diesel::result::Error::NotFound => Self::NotFound(error.to_string()),
            _ => Self::Internal(format!("Database error: {}", error)),
        }
    }
}

// TODO: Differentiate errors.
impl From<reqwest::Error> for Error {
    fn from(error: reqwest::Error) -> Self {
        Error::Incorrect(format!("Non-existent entity: {}", error))
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Error::Incorrect(format!("Incorrect CBOR string encoding: {}", error))
    }
}

impl From<xmltree::ParseError> for Error {
    fn from(error: xmltree::ParseError) -> Self {
        Error::Internal(error.to_string())
    }
}

impl From<url::ParseError> for Error {
    fn from(error: url::ParseError) -> Self {
        Error::Incorrect(format!("Incorrect URI: {}", error))
    }
}

impl From<serde_json::Error> for Error {
    fn from(error: serde_json::Error) -> Self {
        Error::Incorrect(format!("Incorrect JSON encoding: {}", error))
    }
}

impl From<std::num::ParseFloatError> for Error {
    fn from(error: std::num::ParseFloatError) -> Self {
        Error::Incorrect(format!("Incorrect float representation: {}", error))
    }
}

// Called when calling the Mangrove File Server.
impl From<std::str::ParseBoolError> for Error {
    fn from(error: std::str::ParseBoolError) -> Self {
        Error::Internal(error.to_string())
    }
}

impl From<InvalidIri> for Error {
    fn from(error: InvalidIri) -> Self {
        Self::Internal(error.to_string())
    }
}

impl From<TermIndexFullError> for Error {
    fn from(error: TermIndexFullError) -> Self {
        Self::Internal(error.to_string())
    }
}

impl From<InvalidBnodeId> for Error {
    fn from(error: InvalidBnodeId) -> Self {
        Self::Internal(error.to_string())
    }
}

impl From<core::convert::Infallible> for Error {
    fn from(error: core::convert::Infallible) -> Self {
        Error::Internal(error.to_string())
    }
}
