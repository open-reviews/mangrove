use super::database::{self, DbConn};
use super::error::Error;
use super::schema::reviews;
use diesel::prelude::*;
use isbn::Isbn;
use once_cell::sync::Lazy;
use postgis_diesel::types::Point;

use async_once::AsyncOnce;
use aws_config::BehaviorVersion;
use aws_sdk_s3::client::Client as S3Client;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::time::Duration;
use url::Url;

const IMAGES_BUCKET: &str = "files.mangrove.reviews";

static JWT_VALIDATION: Lazy<jsonwebtoken::Validation> = Lazy::new(|| jsonwebtoken::Validation {
    leeway: 5,
    validate_exp: false,
    algorithms: vec![jsonwebtoken::Algorithm::ES256],
    ..Default::default()
});

lazy_static! {
    static ref S3_CLIENT: AsyncOnce<S3Client> = AsyncOnce::new(async {
        let config = aws_config::load_defaults(BehaviorVersion::latest()).await;
        S3Client::new(&config)
    });
}

/// Mangrove Review used for submission or retrieval from the database.
#[derive(Debug, PartialEq, Serialize, Deserialize, Identifiable, Insertable, Queryable)]
#[diesel(table_name = reviews)]
#[diesel[primary_key(signature)]]
pub struct Review {
    /// JWT signature by the review issuer.
    pub signature: String,
    /// Review in JWT format.
    pub jwt: String,
    /// Public key of the reviewer in PEM format.
    pub kid: String,
    /// Primary content of the review.
    #[diesel(embed)]
    pub payload: Payload,
    pub scheme: String,
    #[diesel(embed)]
    pub geo: UncertainPoint,
}

impl<'__s> utoipa::ToSchema<'__s> for Review {
    fn schema() -> (
        &'__s str,
        utoipa::openapi::RefOr<utoipa::openapi::schema::Schema>,
    ) {
        (
            "Review",
            utoipa::openapi::ObjectBuilder::new()
                .description(Some(
                    "Mangrove Review in encoded JWT form and with decoded fields.",
                ))
                .property(
                    "jwt",
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::SchemaType::String)
                        .description(Some("Review in JWT format.")),
                )
                .required("jwt")
                .property(
                    "kid",
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::SchemaType::String)
                        .description(Some("Public key of the reviewer in PEM format.")),
                )
                .required("kid")
                .property(
                    "payload",
                    utoipa::openapi::AllOfBuilder::new()
                        .description(Some("Primary content of the review."))
                        .item(utoipa::openapi::RefOr::Ref(utoipa::openapi::Ref::new(
                            "#/components/schemas/Payload",
                        ))),
                )
                .required("payload")
                .property(
                    "signature",
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::SchemaType::String)
                        .description(Some("JWT signature by the review issuer.")),
                )
                .required("signature")
                .into(),
        )
    }
}

// impl FromStr for Review {
impl Review {
    pub async fn parse(jwt_review: &str) -> Result<Self, Error> {
        // Do not put it up due to generic names.
        use jsonwebtoken::{decode, decode_header, DecodingKey};

        let kid = decode_header(&jwt_review)?
            .kid
            .ok_or_else(|| Error::Incorrect("kid is missing from header".into()))?;

        let payload = decode::<Payload>(
            &jwt_review,
            &DecodingKey::from_ec_pem(kid.as_bytes())?,
            &JWT_VALIDATION,
        )?
        .claims;
        payload.validate().await?;

        let (scheme, geo, _) = validate_sub(&payload.sub).await?;

        Ok(Review {
            jwt: jwt_review.into(),
            kid,
            // Use the JWT structure to find signature.
            signature: jwt_review
                .split('.')
                .nth(2)
                .expect("Assuming jsonwebtoken does validation when decoding above.")
                .into(),
            payload,
            scheme,
            geo,
        })
    }
}

impl Review {
    pub async fn validate_db(&self, conn: &DbConn) -> Result<(), Error> {
        if self.scheme == "urn:maresi" {
            check_maresi(conn, &self.payload.sub).await?;
        }
        let similar = conn
            .filter(database::Query {
                kid: Some(self.kid.clone()),
                sub: Some(self.payload.sub.clone()),
                rating: self.payload.rating,
                opinion: self.payload.opinion.clone(),
                ..Default::default()
            })
            .await?;
        if similar.is_empty() {
            Ok(())
        } else {
            Err(Error::Incorrect(
                "Duplicate entry found in the database, please submit reviews with differing rating or opinion.".into()
            ))
        }
    }
}

/// Primary content of the review, this is what gets serialized for signing.
#[derive(Debug, PartialEq, Serialize, Deserialize, Insertable, Queryable)]
#[diesel[table_name = reviews]]
pub struct Payload {
    /// Unix Time at which the review was signed.
    pub iat: i64,
    /// URI of the subject that is being reviewed.
    pub sub: String,
    /// Rating in range [0, 100] indicating how likely
    /// the issuer is to recommend the subject.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rating: Option<Rating>,
    /// Text of an opinion that the issuer had about the subject.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub opinion: Option<String>,
    /// Hashes referring to additional data,
    /// such as pictures available at https://files.mangrove.reviews/<hash>
    #[serde(skip_serializing_if = "Option::is_none")]
    pub images: Option<serde_json::Value>,
    /// Any data relating to the issuer or circumstances of leaving review.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<serde_json::Value>,
}

impl<'__s> utoipa::ToSchema<'__s> for Payload {
    fn schema() -> (
        &'__s str,
        utoipa::openapi::RefOr<utoipa::openapi::schema::Schema>,
    ) {
        (
            "Payload",
            utoipa::openapi::ObjectBuilder::new()
                .description(Some("Primary content of the review, this is what gets serialized for signing."))
                .property(
                    "iat",
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::SchemaType::Integer)
                        .format(Some(utoipa::openapi::SchemaFormat::KnownFormat(
                            utoipa::openapi::KnownFormat::Int64,
                        )))
                        .description(Some("Unix Time at which the review was signed.")),
                )
                .required("iat")
                .property(
                    "sub",
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::SchemaType::String)
                        .format(Some(utoipa::openapi::SchemaFormat::Custom("uri".to_string())))
                        .description(Some("List of subject URIs to get information about.")),
                )
                .required("sub")
                .property(
                    "rating",
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::SchemaType::Integer)
                        .format(Some(utoipa::openapi::SchemaFormat::KnownFormat(
                            utoipa::openapi::KnownFormat::Int16,
                        )))
                        .nullable(true)
                        .maximum(Some(100_f64))
                        .minimum(Some(0_f64))
                        .description(Some("Rating indicating how likely the issuer is to recommend the subject.")),
                )
                .required("rating")
                .property(
                    "opinion",
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::SchemaType::String)
                        .nullable(true)
                        .description(Some("Text of an opinion that the issuer had about the subject."))
                        .max_length(Some(1000)),
                )
                .required("opinion")
                .property(
                    "images",
                    utoipa::openapi::ArrayBuilder::new()
                        .description(Some("Images to be included in the review, contain URLs and optional labels."))
                        .nullable(true)
                        .items(utoipa::openapi::ObjectBuilder::new()
                            .property("src", utoipa::openapi::ObjectBuilder::new().schema_type(utoipa::openapi::SchemaType::String)
                                .format(Some(utoipa::openapi::SchemaFormat::Custom("url".to_string()))))
                            .property("label", utoipa::openapi::ObjectBuilder::new().schema_type(utoipa::openapi::SchemaType::String))
                        ),
                )
                .required("images")
                .property(
                    "metadata",
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::SchemaType::Object)
                        .nullable(true)
                        .description(Some("Any data relating to the issuer or circumstances of leaving review."))
                        .property("client_id", utoipa::openapi::ObjectBuilder::new().schema_type(utoipa::openapi::SchemaType::String))
                        .property("nickname", utoipa::openapi::ObjectBuilder::new().schema_type(utoipa::openapi::SchemaType::String)),
                )
                .required("metadata")
                .into(),
        )
    }
}

impl Payload {
    /// Check the Review roughly in order of complexity of checks.
    pub async fn validate(&self) -> Result<bool, Error> {
        let images: Option<Images> = match self.images {
            Some(ref v) => serde_json::from_value(v.clone())?,
            None => None,
        };
        let metadata: Option<Metadata> = match self.metadata {
            Some(ref v) => serde_json::from_value(v.clone())?,
            None => None,
        };
        check_timestamp(Duration::from_secs(self.iat as u64))?;
        if self.rating.is_none() && self.opinion.is_none() {
            return Err(Error::Incorrect(
                "Review must contain either a rating or a review.".into(),
            ));
        }
        self.rating.map_or(Ok(()), check_rating)?;
        self.opinion
            .as_ref()
            .map_or(Ok(()), |s| check_opinion(&s))?;

        metadata.map_or(Ok(()), |m| {
            m.0.into_iter().try_for_each(|(k, v)| check_metadata(&k, v))
        })?;

        if let Some(images) = images {
            for image in images.0.iter() {
                image.validate().await?;
            }

            // Make sure images stick around.
            for image in images.0.iter() {
                image.persist().await?;
            }
        }

        Ok(true)
    }
}

pub type Rating = i16;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Metadata(pub BTreeMap<String, serde_json::Value>);

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Images(Vec<Image>);

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Image {
    src: String,
    label: Option<String>,
}

impl Image {
    async fn validate(&self) -> Result<(), Error> {
        if let Some(l) = &self.label {
            if l.len() > 50 {
                return Err(Error::Incorrect(format!("Image label is too long: {}", l)));
            }
        }
        println!("Checking the file: {}", self.src);

        let status = reqwest::get(&self.src).await?.status();

        if status.is_success() || status.is_redirection() {
            Ok(())
        } else {
            Err(Error::Incorrect(format!(
                "No file can be found online: {:?}",
                self.src
            )))
        }
    }

    async fn persist(&self) -> Result<(), Error> {
        let parsed = Url::parse(&self.src)?;
        if parsed.host_str() != Some(IMAGES_BUCKET) {
            // Nothing to be done if the image is not on the original server.
            return Ok(());
        }
        let key = parsed
            .path()
            .get(1..)
            .ok_or_else(|| {
                Error::Incorrect(format!("File hash is too short: {:?}", parsed.path()))
            })?
            .into();
        S3_CLIENT
            .get()
            .await
            .delete_object_tagging()
            .set_bucket(Some(IMAGES_BUCKET.into()))
            .set_key(Some(key))
            .set_version_id(None)
            .send()
            .await?;
        Ok(())
    }
}

#[derive(
    Debug, PartialEq, Default, Serialize, Deserialize, Insertable, Queryable, utoipa::ToSchema,
)]
#[diesel[table_name = reviews]]
pub struct UncertainPoint {
    #[schema(value_type = Object, nullable)]
    pub coordinates: Option<Point>,
    pub uncertainty: Option<i32>,
}

const MANGROVE_EPOCH: Duration = Duration::from_secs(1_577_836_800);

fn check_timestamp(iat: Duration) -> Result<(), Error> {
    if iat < MANGROVE_EPOCH {
        Err(Error::Incorrect(
            "Claim too old (`iat` indicates date lower than year 2020).".into(),
        ))
    } else {
        Ok(())
    }
}

pub const MAX_RATING: Rating = 100;

fn check_rating(rating: Rating) -> Result<(), Error> {
    if rating < 0 || rating > MAX_RATING {
        Err(Error::Incorrect("Rating out of range.".into()))
    } else {
        Ok(())
    }
}

const MAX_REVIEW_LENGTH: usize = 1000;

fn check_opinion(opinion: &str) -> Result<(), Error> {
    if opinion.len() <= MAX_REVIEW_LENGTH {
        Ok(())
    } else {
        Err(Error::Incorrect("Opinion too long.".into()))
    }
}

fn check_geo_param(param: (Cow<str>, Cow<str>)) -> Result<Option<i32>, Error> {
    match param.0.as_ref() {
        "q" => {
            if param.1.len() > 100 {
                Err(Error::Incorrect("Place name too long.".into()))
            } else {
                Ok(None)
            }
        },
        "u" => match param.1.parse::<i32>() {
            Ok(n) if 0 < n && n < 40_000_000 => Ok(Some(n)),
            _ => Err(Error::Incorrect("Uncertainty incorrect.".into())),
        },
        _ => Err(Error::Incorrect("Query field unknown.".into())),
    }
}

/// Check if `geo` URI is correct.
fn check_place(geo: &Url) -> Result<UncertainPoint, Error> {
    let mut coords = geo.path().split(',').map(|c| c.parse());
    let lat = coords
        .next()
        .ok_or_else(|| Error::Incorrect("No latitude found.".into()))??;
    if -90. > lat || lat > 90. {
        return Err(Error::Incorrect("Latitude out of range.".into()));
    }
    let lon = coords
        .next()
        .ok_or_else(|| Error::Incorrect("No longitude found.".into()))??;
    if -180. > lon || lon > 180. {
        return Err(Error::Incorrect("Longitude out of range.".into()));
    }
    let mut pairs = geo.query_pairs().peekable();
    if pairs.peek().is_some() {
        let a = pairs
            .map(check_geo_param)
            .collect::<Result<Vec<Option<i32>>, Error>>()?;
        if let Some(uncertainty) = a.into_iter().find(Option::is_some) {
            Ok(UncertainPoint {
                coordinates: Some(Point {
                    x: lon,
                    y: lat,
                    srid: Some(4326),
                }),
                uncertainty,
            })
        } else {
            Err(Error::Incorrect(
                "Geo URI has to specify uncertainty.".into(),
            ))
        }
    } else {
        Err(Error::Incorrect("Geo URI has to specify query.".into()))
    }
}

async fn check_url(uri: &Url) -> Result<(), Error> {
    let exists = reqwest::get(uri.as_str()).await?.status().is_success();
    if exists {
        Ok(())
    } else {
        Err(Error::Incorrect(format!(
            "Subject is not publicly accessible: {}",
            uri
        )))
    }
}

async fn check_lei(id: &str) -> Result<(), Error> {
    if reqwest::get(&format!("https://api.gleif.org/api/v1/lei-records/{}", id))
        .await?
        .status()
        .is_success()
    {
        Ok(())
    } else {
        Err(Error::Incorrect(
            "LEI not found in the GLEIF database.".into(),
        ))
    }
}

fn check_isbn(id: &str) -> Result<(), Error> {
    if id.chars().any(|c| c.is_whitespace()) {
        Err(Error::Incorrect(
            "ISBN should not contain whitespace characters.".into(),
        ))
    } else {
        match id.parse::<Isbn>() {
            Ok(_) => Ok(()),
            Err(e) => Err(Error::Incorrect(format!("ISBN incorrect: {:?}", e))),
        }
    }
}

/// Check Mangrove Review Signature, which is a unique id of a review.
async fn check_maresi(conn: &DbConn, sub: &str) -> Result<(), Error> {
    if let Some(id) = sub.split(':').nth(2) {
        conn.select(id).await.map(|_| ())
    } else {
        Err(Error::Incorrect(format!("No signature found: {}", sub)))
    }
}

pub async fn validate_sub(sub: &str) -> Result<(String, UncertainPoint, Option<String>), Error> {
    let uri = Url::parse(&sub)?;
    match uri.scheme() {
        "urn" => {
            let sub = Url::parse(uri.path())?;
            // Parsing lower cases the scheme.
            match sub.scheme() {
                "lei" => check_lei(sub.path()).await,
                "isbn" => check_isbn(sub.path()),
                // This scheme is only checked with database.
                "maresi" => Ok(()),
                s => Err(Error::Incorrect(format!("Unknown URN scheme: {}", s))),
            }?;
            Ok((
                format!("{}:{}", uri.scheme(), sub.scheme()),
                Default::default(),
                None,
            ))
        },
        "geo" => check_place(&uri)
            .map(|p| {
                (
                    uri.scheme().into(),
                    p,
                    uri.query_pairs()
                        .find(|(query, _)| query == "q")
                        .map(|(_, value)| value.to_string()),
                )
            })
            .map_err(Into::into),
        "https" => check_url(&uri)
            .await
            .map(|_| (uri.scheme().into(), Default::default(), None))
            .map_err(Into::into),
        s => Err(Error::Incorrect(format!("Unknown URI scheme: {}", s))),
    }
}

fn check_short_string(key: &str, value: serde_json::Value) -> Result<(), Error> {
    if serde_json::from_value::<String>(value)?.len() > 20 {
        Err(Error::Incorrect(format!("Field {} is too long.", key)))
    } else {
        Ok(())
    }
}

fn check_uri(key: &str, value: serde_json::Value) -> Result<(), Error> {
    let uri = serde_json::from_value::<String>(value)?;
    match Url::parse(&uri) {
        Ok(_) => Ok(()),
        Err(e) => Err(Error::Incorrect(format!(
            "Unable to parse URI for {}: {}",
            key, e
        ))),
    }
}

fn check_flag(key: &str, value: serde_json::Value) -> Result<(), Error> {
    match value.as_str() {
        Some("true") => Ok(()),
        _ => Err(Error::Incorrect(format!(
            "Flag field {} can only have value equal to `true`",
            key
        ))),
    }
}

fn check_metadata(key: &str, value: serde_json::Value) -> Result<(), Error> {
    match key {
        "client_id" => check_uri(key, value),
        "nickname" => check_short_string(key, value),
        "age" => match value.as_u64() {
            Some(n) if n <= 200 => Ok(()),
            _ => Err(Error::Incorrect("Provided age is incorrect.".into())),
        },
        "experience_context" => check_short_string(key, value),
        "openid" => check_short_string(key, value),
        "data_source" => check_uri(key, value),
        "issuer_index" => match value.as_u64() {
            Some(n) if n <= 9_007_199_254_740_991 => Ok(()),
            _ => Err(Error::Incorrect("Provided index is incorrect.".into())),
        },
        "preferred_username" => check_short_string(key, value),
        "birthdate" => check_short_string(key, value),
        "family_name" => check_short_string(key, value),
        "given_name" => check_short_string(key, value),
        "gender" => check_short_string(key, value),
        "is_generated" => check_flag(key, value),
        "is_affiliated" => check_flag(key, value),
        "is_personal_experience" => check_flag(key, value),
        _ => Err(Error::Incorrect(
            "Key is not one of Mangrove Core Metadata Keys.".into(),
        )),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_uri() {
        check_place(&Url::parse("geo:0,0?u=30&q=47.1691576,8.514572(Juanitos)").unwrap()).unwrap();
        assert!(
            check_place(&Url::parse("geo:?u=30&q=47.1691576,8.514572(Juanitos)").unwrap()).is_err()
        );
        assert!(check_place(&Url::parse("geo:37.78,-122.399").unwrap()).is_err());
    }
}
