use super::aggregator_schema::{reviewers, subjects};
use super::database::{DbConn, Query};
use super::error::Error;
use super::review::MAX_RATING;
use diesel::prelude::*;
use diesel_async::RunQueryDsl;
use serde::Serialize;
use std::collections::BTreeMap;

pub trait Statistic {
    fn compute(
        conn: &DbConn,
        identifier: String,
    ) -> impl std::future::Future<Output = Result<Self, Error>> + Send
    where
        Self: Sized + Sync + Send + 'static;
    fn compute_bulk(
        conn: &DbConn,
        identifiers: impl Iterator<Item = String> + Send,
    ) -> impl std::future::Future<Output = Result<BTreeMap<String, Self>, Error>> + Send
    where
        Self: Sized + Sync + Send + 'static,
    {
        async move {
            let mut out = BTreeMap::new();
            for identifier in identifiers {
                let conn = conn.clone();
                let res = Self::compute(&conn, identifier.clone())
                    .await
                    .map(|statistic| (identifier, statistic))?;
                out.insert(res.0, res.1);
            }

            Ok(out)
        }
    }
}

/// Information about a subject of reviews.
#[derive(Debug, Serialize, utoipa::ToSchema)]
pub struct Subject {
    /// URI uniquely identifying the subject.
    #[schema(value_type = String, format = "uri", required = true)]
    pub sub: String,
    /// Aggregate number representing quality of the subject.
    #[schema(value_type = i16, format = "int16", required = true, nullable = true)]
    pub quality: Option<i16>,
    /// Number of reviews given to this subject.
    #[schema(value_type = usize, format = "uint", minimum = 0, required = true)]
    pub count: usize,
    /// Number of reviews which included an opinion.
    #[schema(value_type = usize, format = "uint", minimum = 0, required = true)]
    pub opinion_count: usize,
    /// Number of reviews with rating above 50 given to this subject.
    #[schema(value_type = usize, format = "uint", minimum = 0, required = true)]
    pub positive_count: usize,
    /// Number of reviews with rating above 50 and `is_personal_experience` flag given to this subject.
    #[schema(value_type = usize, format = "uint", minimum = 0, required = true)]
    pub confirmed_count: usize,
}

impl Statistic for Subject {
    async fn compute(conn: &DbConn, sub: String) -> Result<Self, Error> {
        let relevant = conn
            .filter(Query {
                sub: Some(sub.clone()),
                ..Default::default()
            })
            .await?;
        let mut count = 0;
        let mut opinion_count = 0;
        let mut positive_count = 0;
        let mut confirmed_count = 0;
        for review in relevant {
            count += 1;
            if let Some(rating) = review.payload.rating {
                if rating > MAX_RATING / 2 {
                    positive_count += 1;
                    if let Some(metadata) = review.payload.metadata {
                        if metadata.get("is_personal_experience").is_some() {
                            confirmed_count += 1;
                        }
                    }
                }
            }
            if review.payload.opinion.is_some() {
                opinion_count += 1;
            }
        }
        let mut conn = conn.get_connection().await?;
        let quality = subjects::table
            .find(&sub)
            .select(subjects::quality)
            .first::<i16>(&mut conn)
            .await
            .optional()?;
        Ok(Subject {
            sub,
            quality,
            count,
            opinion_count,
            positive_count,
            confirmed_count,
        })
    }
}

/// Information about a review issuer.
#[derive(Debug, Serialize, utoipa::ToSchema)]
pub struct Issuer {
    /// Number of reviews written by this issuer.
    pub count: usize,
    /// How likely is it for this reviewer to be fair.
    #[schema(minimum = 0, maximum = 1, nullable = false)]
    pub neutrality: Option<f32>,
}

impl Statistic for Issuer {
    async fn compute(conn: &DbConn, kid: String) -> Result<Self, Error> {
        let mut connection = conn.get_connection().await?;
        let neutrality = reviewers::table
            .find(&kid)
            .select(reviewers::neutrality)
            .first::<f32>(&mut connection)
            .await
            .optional()?;
        let count = conn
            .filter(Query {
                kid: Some(kid),
                ..Default::default()
            })
            .await?
            .len();
        Ok(Issuer { count, neutrality })
    }
}
