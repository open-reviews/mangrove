# Payload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iat** | **i64** | Unix Time at which the review was signed. | 
**sub** | **String** | URI of the subject that is being reviewed. | 
**rating** | Option<**i32**> | Rating indicating how likely the issuer is to recommend the subject. | 
**opinion** | Option<**String**> | Text of an opinion that the issuer had about the subject. | 
**images** | Option<[**Vec<crate::models::PayloadImagesInner>**](Payload_images_inner.md)> | Images to be included in the review, contain URLs and optional labels. | 
**metadata** | Option<[**crate::models::PayloadMetadata**](Payload_metadata.md)> |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


