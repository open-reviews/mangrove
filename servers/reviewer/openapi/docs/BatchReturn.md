# BatchReturn

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issuers** | Option<[**::std::collections::HashMap<String, crate::models::Issuer>**](Issuer.md)> |  | 
**subjects** | Option<[**::std::collections::HashMap<String, crate::models::Subject>**](Subject.md)> |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


