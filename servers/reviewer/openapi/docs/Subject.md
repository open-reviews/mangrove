# Subject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**confirmed_count** | **i32** | Number of reviews with rating above 50 and `is_personal_experience` flag given to this subject. | 
**count** | **i32** | Number of reviews given to this subject. | 
**opinion_count** | **i32** | Number of reviews which included an opinion. | 
**positive_count** | **i32** | Number of reviews with rating above 50 given to this subject. | 
**quality** | Option<**i32**> | Aggregate number representing quality of the subject. | 
**sub** | **String** | URI uniquely identifying the subject. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


