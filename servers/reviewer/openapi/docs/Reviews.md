# Reviews

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issuers** | Option<[**::std::collections::HashMap<String, crate::models::Issuer>**](Issuer.md)> | A map from public keys to information about issuers. | 
**maresi_subjects** | Option<[**::std::collections::HashMap<String, crate::models::Subject>**](Subject.md)> | A map from Review identifiers (`urn:maresi:<signature>`) to information about the reviews of that review. | 
**reviews** | [**Vec<crate::models::Review>**](Review.md) | A list of reviews satisfying the query. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


