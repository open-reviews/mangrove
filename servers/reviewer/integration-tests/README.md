# Integration tests for reviewer server
### Environment variables
Pre-defined environment variable:
- BASE_PATH (default: "http://localhost:8000")

### Requirements
- openapi generated client lib (default "../openapi")

### Run Test sequence
Run the command: `cargo run`
