// ########################################
// Test case Request aggregate info by pem
// ########################################

use super::{TestSequenceResult, CONFIGURATION, TEST_REVIEW};
use crate::apis::default_api::issuer_pem_get;
use anyhow::{anyhow, Result as AnyhowResult};

// Test 1 Request aggregate by pem
pub async fn test_request_aggregate_info_pem_success() -> AnyhowResult<TestSequenceResult> {
    let pem = TEST_REVIEW.kid.as_str();
    issuer_pem_get(&CONFIGURATION, pem)
        .await
        .map(TestSequenceResult::Issuer)
        .map_err(|err| anyhow!(err))
}

// Test 2 Request aggregate with wrong pem
// Fail Ok
pub async fn test_request_aggregate_info_wrong_pem() -> AnyhowResult<TestSequenceResult> {
    let pem = "-----BEGIN PUBLIC KEY-----test_request_aggregate_wrong_pem-----END PUBLIC KEY-----";
    issuer_pem_get(&CONFIGURATION, pem)
        .await
        .map(TestSequenceResult::Issuer)
        .map_err(|err| anyhow!(err))
}
