// ############################################
// Test case Request aggregate info by subject
// ############################################

use super::{TestSequenceResult, CONFIGURATION, TEST_REVIEW};
use crate::apis::default_api::subject_sub_get;
use anyhow::{anyhow, Result as AnyhowResult};

// Test 1 request aggregate by subject
pub async fn test_request_aggregate_success() -> AnyhowResult<TestSequenceResult> {
    let sub = TEST_REVIEW.payload.sub.as_str();
    subject_sub_get(&CONFIGURATION, sub)
        .await
        .map(TestSequenceResult::Subject)
        .map_err(|err| anyhow!(err))
}

// Test 2 request aggregate by invalid subject
// Fail Ok
pub async fn test_request_aggregate_invalid_sub() -> AnyhowResult<TestSequenceResult> {
    let sub = "request_aggregate_invalid_sub";
    match subject_sub_get(&CONFIGURATION, sub).await {
        Ok(val) => Err(anyhow!(format!("{:?}", val))),
        Err(err) => Ok(TestSequenceResult::SubjectSubGetError(err.to_string())),
    }
}
