// #########################################
// Test case Request Review query parameter
// #########################################

use super::{TestSequenceResult, CONFIGURATION, TEST_REVIEW};
use crate::apis::default_api::reviews_get;
use crate::test_cases::configuration::SUB_TEST_PARAMETER_Q;
use anyhow::{anyhow, Result as AnyhowResult};

// Test 1 query by q parameter
pub async fn test_request_review_q() -> AnyhowResult<TestSequenceResult> {
    //Result<crate::models::Reviews, Error<ReviewsGetError>> {
    let q = TEST_REVIEW.payload.sub.as_str();
    reviews_get(
        &CONFIGURATION,
        Some(q),
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    )
    .await
    .map(TestSequenceResult::Reviews)
    .map_err(|err| anyhow!(err))
}

// Test 2 query by signature parameter
pub async fn test_request_review_signature() -> AnyhowResult<TestSequenceResult> {
    let signature = TEST_REVIEW.signature.as_str();
    reviews_get(
        &CONFIGURATION,
        None,
        Some(signature),
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    )
    .await
    .map(TestSequenceResult::Reviews)
    .map_err(|err| anyhow!(err))
}

// Test 3 query by kid parameter
pub async fn test_request_review_kid() -> AnyhowResult<TestSequenceResult> {
    let kid = TEST_REVIEW.kid.as_str();
    reviews_get(
        &CONFIGURATION,
        None,
        None,
        Some(kid),
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    )
    .await
    .map(TestSequenceResult::Reviews)
    .map_err(|err| anyhow!(err))
}

// Test 4 query by iat parameter
pub async fn test_request_review_iat() -> AnyhowResult<TestSequenceResult> {
    let iat = TEST_REVIEW.payload.iat;
    reviews_get(
        &CONFIGURATION,
        None,
        None,
        None,
        Some(iat),
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    )
    .await
    .map(TestSequenceResult::Reviews)
    .map_err(|err| anyhow!(err))
}

// Test 5 query by sub parameter
pub async fn test_request_review_sub() -> AnyhowResult<TestSequenceResult> {
    let sub = TEST_REVIEW.payload.sub.as_str();
    reviews_get(
        &CONFIGURATION,
        None,
        None,
        None,
        None,
        None,
        Some(sub),
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    )
    .await
    .map(TestSequenceResult::Reviews)
    .map_err(|err| anyhow!(err))
}

// Test 6 query by sub parameter checking search by q parameter
pub async fn test_request_review_sub_q_parameter() -> AnyhowResult<TestSequenceResult> {
    let sub = SUB_TEST_PARAMETER_Q.as_str();
    reviews_get(
        &CONFIGURATION,
        None,
        None,
        None,
        None,
        None,
        Some(sub),
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    )
    .await
    .map(TestSequenceResult::Reviews)
    .map_err(|err| anyhow!(err))
}

// Test 7 query by opinion parameter
pub async fn test_request_review_opinion() -> AnyhowResult<TestSequenceResult> {
    let opinion = TEST_REVIEW.payload.opinion.as_ref().unwrap().as_str();
    reviews_get(
        &CONFIGURATION,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        Some(opinion),
        None,
        None,
        None,
        None,
        None,
    )
    .await
    .map(TestSequenceResult::Reviews)
    .map_err(|err| anyhow!(err))
}

// Test 8 query by several parameters
pub async fn test_request_review_complex() -> AnyhowResult<TestSequenceResult> {
    let signature = TEST_REVIEW.signature.as_str();
    let opinion = TEST_REVIEW.payload.opinion.as_ref().unwrap().as_str();
    let sub = TEST_REVIEW.payload.sub.as_str();
    let iat = TEST_REVIEW.payload.iat;
    let kid = TEST_REVIEW.kid.as_str();

    reviews_get(
        &CONFIGURATION,
        None,
        Some(signature),
        Some(kid),
        Some(iat),
        None,
        Some(sub),
        None,
        Some(opinion),
        None,
        None,
        None,
        None,
        None,
    )
    .await
    .map(TestSequenceResult::Reviews)
    .map_err(|err| anyhow!(err))
}
