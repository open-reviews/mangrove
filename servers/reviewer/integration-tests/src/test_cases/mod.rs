mod configuration;
mod request_aggregate_info_pem;
mod request_aggregate_info_sub;
mod request_aggregates_multiple_subjects_or_issue;
mod request_review;
mod request_review_by_coordinates;
mod request_review_signature;
mod submit_review;

use crate::test_cases::submit_review::{
    test_submit_invalid_review, test_submit_review_duplication, test_submit_review_success,
    test_submit_second_review_success, test_submit_third_review_success,
};
pub use configuration::{
    CONFIGURATION, TEST_BATCH_QUERY_PEMS, TEST_BATCH_QUERY_PEMS_SUBS, TEST_BATCH_QUERY_SUBS,
    TEST_REVIEW, TEST_REVIEWS, TEST_REVIEW_SECOND, TEST_REVIEW_THIRD,
};

use crate::test_cases::request_review::{
    test_request_review_complex, test_request_review_iat, test_request_review_kid,
    test_request_review_opinion, test_request_review_q, test_request_review_signature,
    test_request_review_sub, test_request_review_sub_q_parameter,
};
use anyhow::{anyhow, Result as AnyhowResult};
use openapi::models::{BatchReturn, Issuer, Review, Reviews, Subject};
use std::collections::HashMap;
use std::future::Future;
use std::pin::Pin;

use crate::test_cases::configuration::{
    REVIEWS_EMPTY, REVIEWS_THIRD_REVIEW, TEST_BATCH_RETURN_PEMS, TEST_BATCH_RETURN_PEMS_SUBS,
    TEST_BATCH_RETURN_SUBS, TEST_ISSUER_EXAMPLE_ORG, TEST_ISSUER_NOT_FOUND,
    TEST_SUBJECT_EXAMPLE_ORG,
};
use crate::test_cases::request_aggregate_info_pem::{
    test_request_aggregate_info_pem_success, test_request_aggregate_info_wrong_pem,
};
use crate::test_cases::request_aggregate_info_sub::{
    test_request_aggregate_invalid_sub, test_request_aggregate_success,
};
use crate::test_cases::request_aggregates_multiple_subjects_or_issue::{
    test_batch_query_pems_subs_success, test_batch_query_pems_success,
    test_batch_query_subs_success,
};
use crate::test_cases::request_review_by_coordinates::{
    test_request_review_by_coordinates_success,
    test_request_review_by_coordinates_success_boundary, test_request_review_by_coordinates_wrong,
};
use crate::test_cases::request_review_signature::{
    test_request_review_signature_invalid, test_request_review_signature_success,
};
use colored::Colorize;
use lazy_static::lazy_static;
use tokio::sync::Mutex;

lazy_static! {
    pub static ref TEST_SEQUENCE_ERRORS_COUNT: Mutex<u16> = Mutex::new(0);
}

#[derive(Debug, PartialEq)]
pub(crate) enum TestSequenceResult {
    Boolean(bool),
    Review(Review),
    Reviews(Reviews),
    Subject(Subject),
    Issuer(Issuer),
    BatchReturn(BatchReturn),
    SubjectSubGetError(String),
    SubmitJwtReviewPutError(String),
    ReviewSignatureGetError(String),
}

fn create_test_sequence_expected_results() -> HashMap<&'static str, TestSequenceResult> {
    HashMap::from([
        //#######################################
        //## Submit review test cases' results ##
        //#######################################
        (
            "test_submit_review_success",
            TestSequenceResult::Boolean(true),
        ),
        (
            "test_submit_second_review_success",
            TestSequenceResult::Boolean(true),
        ),
        (
            "test_submit_third_review_success",
            TestSequenceResult::Boolean(true)
        ),
        (
            "test_submit_review_duplication",
            TestSequenceResult::SubmitJwtReviewPutError(
                "error in response: status code 400 Bad Request, text Duplicate entry found in the database, please submit reviews with differing rating or opinion."
                    .to_owned(),
            ),
        ),
        (
            "test_submit_invalid_review",
            TestSequenceResult::SubmitJwtReviewPutError(
                "error in response: status code 400 Bad Request, text kid is missing from header"
                    .to_owned(),
            ),
        ),
        //#######################################################
        //## Results Test cases Request Review query parameter ##
        //#######################################################
        (
            "test_request_review_q",
            TestSequenceResult::Reviews(TEST_REVIEWS.clone())
        ),
        (
            "test_request_review_signature",
            TestSequenceResult::Reviews(TEST_REVIEWS.clone())
        ),
        (
            "test_request_review_kid",
            TestSequenceResult::Reviews(TEST_REVIEWS.clone())
        ),
        (
            "test_request_review_iat",
            TestSequenceResult::Reviews(TEST_REVIEWS.clone())
        ),
        (
            "test_request_review_sub",
            TestSequenceResult::Reviews(TEST_REVIEWS.clone())
        ),
        (
            "test_request_review_sub_q_parameter",
            TestSequenceResult::Reviews(REVIEWS_THIRD_REVIEW.clone())
        ),
        (
            "test_request_review_opinion",
            TestSequenceResult::Reviews(TEST_REVIEWS.clone())
        ),
        (
            "test_request_review_complex",
            TestSequenceResult::Reviews(TEST_REVIEWS.clone())
        ),
        //#####################################################
        //## Results Test case Request Review signature path ##
        //#####################################################
        (
            "test_request_review_signature_success",
            TestSequenceResult::Review(TEST_REVIEW.clone())
        ),
        (
            "test_request_review_signature_invalid",
            TestSequenceResult::ReviewSignatureGetError(
                "error in response: status code 404 Not Found, text Record not found".to_owned()
            )
        ),
        // #########################################################
        // ## Results Test case Request aggregate info by subject ##
        // #########################################################
        (
            "test_request_aggregate_success",
            TestSequenceResult::Subject(TEST_SUBJECT_EXAMPLE_ORG.clone())
        ),
        (
            "test_request_aggregate_invalid_sub",
            TestSequenceResult::SubjectSubGetError("error in response: status code 400 Bad Request, text Incorrect URI: relative URL without a base".to_owned())
        ),
        // #####################################################
        // ## Results Test case Request aggregate info by pem ##
        // #####################################################
        (
            "test_request_aggregate_info_pem_success",
            TestSequenceResult::Issuer(TEST_ISSUER_EXAMPLE_ORG.clone()),
        ),
        (
            "test_request_aggregate_info_wrong_pem",
            TestSequenceResult::Issuer(TEST_ISSUER_NOT_FOUND.clone()),
        ),
        // #########################################################################
        // ## Results Test case Request aggregates for multiple subjects or issue ##
        // #########################################################################
        (
            "test_batch_query_pems_subs_success",
            TestSequenceResult::BatchReturn(TEST_BATCH_RETURN_PEMS_SUBS.clone())
        ),
        (
            "test_batch_query_pems_success",
            TestSequenceResult::BatchReturn(TEST_BATCH_RETURN_PEMS.clone())
        ),
        (
            "test_batch_query_subs_success",
            TestSequenceResult::BatchReturn(TEST_BATCH_RETURN_SUBS.clone())
        ),
        // #####################################################
        // ## Results Test case Request Review by coordinates ##
        // #####################################################
        (
            "test_request_review_by_coordinates_success",
            TestSequenceResult::Reviews(REVIEWS_THIRD_REVIEW.clone())
            ),
        (
            "test_request_review_by_coordinates_success_boundary",
            TestSequenceResult::Reviews(REVIEWS_THIRD_REVIEW.clone())
            ),
        (
            "test_request_review_by_coordinates_wrong",
            TestSequenceResult::Reviews(REVIEWS_EMPTY.clone())
            )
    ])
}

type TestFunction = Pin<Box<dyn Future<Output = AnyhowResult<TestSequenceResult>>>>;

fn create_test_sequence() -> Vec<(&'static str, TestFunction)> {
    vec![
        //##############################
        //## Submit review test cases ##
        //##############################
        (
            "test_submit_review_success",
            Box::pin(test_submit_review_success()),
        ),
        (
            "test_submit_second_review_success",
            Box::pin(test_submit_second_review_success()),
        ),
        (
            "test_submit_third_review_success",
            Box::pin(test_submit_third_review_success()),
        ),
        (
            "test_submit_review_duplication",
            Box::pin(test_submit_review_duplication()),
        ),
        (
            "test_submit_invalid_review",
            Box::pin(test_submit_invalid_review()),
        ),
        //###############################################
        //## Test cases Request Review query parameter ##
        //###############################################
        ("test_request_review_q", Box::pin(test_request_review_q())),
        (
            "test_request_review_signature",
            Box::pin(test_request_review_signature()),
        ),
        (
            "test_request_review_kid",
            Box::pin(test_request_review_kid()),
        ),
        (
            "test_request_review_iat",
            Box::pin(test_request_review_iat()),
        ),
        (
            "test_request_review_sub",
            Box::pin(test_request_review_sub()),
        ),
        (
            "test_request_review_sub_q_parameter",
            Box::pin(test_request_review_sub_q_parameter()),
        ),
        (
            "test_request_review_opinion",
            Box::pin(test_request_review_opinion()),
        ),
        (
            "test_request_review_complex",
            Box::pin(test_request_review_complex()),
        ),
        //##############################################
        //## Test cases Request Review signature path ##
        //##############################################
        (
            "test_request_review_signature_success",
            Box::pin(test_request_review_signature_success()),
        ),
        (
            "test_request_review_signature_invalid",
            Box::pin(test_request_review_signature_invalid()),
        ),
        // #################################################
        // ## Test case Request aggregate info by subject ##
        // #################################################
        (
            "test_request_aggregate_success",
            Box::pin(test_request_aggregate_success()),
        ),
        (
            "test_request_aggregate_invalid_sub",
            Box::pin(test_request_aggregate_invalid_sub()),
        ),
        // #############################################
        // ## Test case Request aggregate info by pem ##
        // #############################################
        (
            "test_request_aggregate_info_pem_success",
            Box::pin(test_request_aggregate_info_pem_success()),
        ),
        (
            "test_request_aggregate_info_wrong_pem",
            Box::pin(test_request_aggregate_info_wrong_pem()),
        ),
        // #################################################################
        // ## Test case Request aggregates for multiple subjects or issue ##
        // #################################################################
        (
            "test_batch_query_pems_subs_success",
            Box::pin(test_batch_query_pems_subs_success()),
        ),
        (
            "test_batch_query_pems_success",
            Box::pin(test_batch_query_pems_success()),
        ),
        (
            "test_batch_query_subs_success",
            Box::pin(test_batch_query_subs_success()),
        ),
        // #####################################################
        // ## Results Test case Request Review by coordinates ##
        // #####################################################
        (
            "test_request_review_by_coordinates_success",
            Box::pin(test_request_review_by_coordinates_success()),
        ),
        (
            "test_request_review_by_coordinates_success_boundary",
            Box::pin(test_request_review_by_coordinates_success_boundary()),
        ),
        (
            "test_request_review_by_coordinates_wrong",
            Box::pin(test_request_review_by_coordinates_wrong()),
        ),
    ]
}

macro_rules! compare_result {
    ($test_name:expr, $actual_result:expr, $expected_results:expr) => {{
        match $expected_results.get($test_name) {
            Some(expected_result) => match &$actual_result {
                Ok(actual) => {
                    if actual == expected_result {
                        println!("{} {}", "Test passed:".green(), $test_name.bold().green());
                    } else {
                        println!(
                            "{} {} {} {}",
                            "Test failed:".red(),
                            $test_name.red().bold(),
                            format!("\nExpected: {:?}", expected_result).yellow(),
                            format!("\nActual: {:?}", actual).color("grey").bold()
                        );
                        *TEST_SEQUENCE_ERRORS_COUNT.lock().await += 1;
                    }
                },
                Err(err) => {
                    println!(
                        "{} {} {} {}",
                        "Test failed:".red(),
                        $test_name.bold().red(),
                        "Error:".red(),
                        format!("{:?}", err).red()
                    );
                    *TEST_SEQUENCE_ERRORS_COUNT.lock().await += 1;
                },
            },
            None => {
                println!("No expected result found for test: {}", $test_name);
                *TEST_SEQUENCE_ERRORS_COUNT.lock().await += 1;
            },
        }
    }};
}

pub async fn run_test_sequence() -> anyhow::Result<()> {
    let test_functions = create_test_sequence();
    let expected_results = create_test_sequence_expected_results();
    let tests_total = test_functions.len();

    for (index, (test_name, test_function)) in test_functions.into_iter().enumerate() {
        println!(
            "\n\n{} {}",
            format!("Running test #{}:", index + 1).italic(),
            test_name.bold()
        );
        let actual_result = test_function.await;
        compare_result!(test_name, actual_result, expected_results);
    }

    let errors_count = *TEST_SEQUENCE_ERRORS_COUNT.lock().await;

    if errors_count > 0 {
        println!(
            "\n\n{}",
            format!("Test finished: {} errors of {}.", errors_count, tests_total)
                .bold()
                .red()
        );
        Err(anyhow!(errors_count))
    } else {
        println!(
            "\n\n{}",
            format!("Test PASSED. Finished {} test cases.", tests_total)
                .bold()
                .green()
        );
        Ok(())
    }
}
