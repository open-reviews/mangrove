// ########################################
// Test case Request Review signature path
// ########################################

use super::{TestSequenceResult, CONFIGURATION, TEST_REVIEW};
use crate::apis::default_api::review_signature_get;
use anyhow::{anyhow, Result as AnyhowResult};

// Test 1 request review by signature parameter
pub async fn test_request_review_signature_success() -> AnyhowResult<TestSequenceResult> {
    let signature = TEST_REVIEW.signature.as_str();
    review_signature_get(&CONFIGURATION, signature)
        .await
        .map(TestSequenceResult::Review)
        .map_err(|err| anyhow!(err))
}

// Test 2 request review by signature parameter
// Fail Ok
pub async fn test_request_review_signature_invalid() -> AnyhowResult<TestSequenceResult> {
    let signature = "invalid_signature";
    match review_signature_get(&CONFIGURATION, signature).await {
        Ok(val) => Err(anyhow!(format!("{:?}", val))),
        Err(err) => Ok(TestSequenceResult::ReviewSignatureGetError(err.to_string())),
    }
}
